<?php

namespace App\Domain\Company\Listeners;

use App\Domain\Company\Events\CompanyCreated;
use App\Domain\User\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendCompanyCreateNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompanyCreated  $event
     * @return void
     */
    public function handle(CompanyCreated $event)
    {
        $company = $event->company;
        $users = User::all();
        foreach ($users as $user) {
            Mail::send([], [], function ($message) use ($user, $company) {
                $message->to($user->email)
                ->subject('New Company Created')
                    ->setBody("
                                <p> hello {$user->name}, </p><br/>
                                <p> A new company '{$company->name}' was successfull created! </p><br/>

                                regards, <br/>
                                Administrator
                                "
                        , 'text/html');
            });
        }
    }
}
