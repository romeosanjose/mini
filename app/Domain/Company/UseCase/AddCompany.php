<?php

namespace App\Domain\Company\UseCase;

use App\Domain\Company\Events\CompanyCreated;
use App\Domain\UseCase;
use Illuminate\Http\Request;
use App\Domain\Company\Employee;

class AddCompany extends UseCase
{
    public function addCompany(Request $request)
    {
        $this->model->name = $request->get('name');
        $this->model->email = $request->get('email');
        $this->model->website = $request->get('website');

        $this->model->save();
        if ($request->hasFile('image')){
            $this->model->logo = $this->processCompanyLogo($this->model->id, $request);
            $this->model->save();
        }

        event(new CompanyCreated($this->model));
        return $this->model;
    }

    protected function processCompanyLogo($companyId, Request $request)
    {
        try{
                $imageName = $companyId . '.' .
                    $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(
                    base_path() . '/storage/app/public/images/company/logo', $imageName
                );

            return $imageName;
        }catch(\Exception $e){
            return null;
        }
    }
}
