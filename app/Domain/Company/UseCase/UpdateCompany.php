<?php


namespace App\Domain\Company\UseCase;


class UpdateCompany extends AddCompany
{
    public function updateCompany($id, $request)
    {
        $company = $this->model::find($id);
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->website = $request->get('website');
        $company->save();
        if ($request->hasFile('image')) {
            $company->logo = $this->processCompanyLogo($company->id, $request);
            $company->save();
        }

        return $company;
    }
}
