<?php

namespace App\Domain\Employee;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone'
    ];

    public function company()
    {
        return $this->belongsTo('App\Domain\Company\Company');
    }
}
