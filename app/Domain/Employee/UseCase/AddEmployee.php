<?php

namespace App\Domain\Employee\UseCase;

use App\Domain\UseCase;
use Illuminate\Http\Request;
use App\Domain\Employee\Employee;

class AddEmployee extends UseCase
{
    public function addEmployee(Request $request)
    {
        $this->model->firstname = $request->get('firstname');
        $this->model->lastname = $request->get('lastname');
        $this->model->company = $request->get('company');
        $this->model->email = $request->get('email');
        $this->model->phone = $request->get('phone');

        $this->model->save();

        return $this->model;
    }

}
