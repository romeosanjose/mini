<?php


namespace App\Domain\Employee\UseCase;


class UpdateEmployee extends AddEmployee
{
    public function updateEmployee($id, $request)
    {
        $employee = $this->model::find($id);
        $employee->firstname = $request->get('firstname');
        $employee->lastname = $request->get('lastname');
        $employee->company = $request->get('company');
        $employee->email = $request->get('email');
        $employee->phone = $request->get('phone');
        $employee->save();

        return $employee;
    }
}
