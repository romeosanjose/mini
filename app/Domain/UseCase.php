<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;

abstract class UseCase{

    protected $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function getAll($page = null) {
        return ($page == null ) ? $this->model::all() :$this->model::paginate($page);
    }

    public function getOne($id) {
        return $this->model::find($id);
    }

    public function deleteOne($id) {
        return $this->model::destroy($id);
    }
}
