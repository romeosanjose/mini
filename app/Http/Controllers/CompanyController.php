<?php

namespace App\Http\Controllers;

use App\Domain\Company\UseCase\DeleteCompany;
use App\Domain\Company\UseCase\GetCompany;
use App\Domain\Company\UseCase\UpdateCompany;
use Illuminate\Http\Request;
use App\Domain\Company\UseCase\AddCompany;
use App\Domain\Company\Company;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $companies = (new GetCompany(new Company()))->getAll(10);
        return view('company.index', ['companies' => $companies]);
    }

    public function create()
    {
        return view('company.create');
    }

    public function edit($id)
    {
        $company = (new GetCompany(new Company()))->getOne($id);
        return view('company.edit', ['company' => $company]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'image' => 'dimensions:min_width=100,min_height=100',
            'email' => 'email'
        ]);
        $company = (new AddCompany(new Company()))->addCompany($request);
        return redirect()->route('company')
                ->with('message', "Company '$company->name' was successfully added");
    }

    public function update($id,Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'image' => 'dimensions:min_width=100,min_height=100',
            'email' => 'email'
        ]);
        $company = (new UpdateCompany(new Company()))->updateCompany($id, $request);
        return redirect()->route('company')
            ->with('message', "Company '$company->name' was successfully updated");
    }

    public function delete($id)
    {
        try {
            (new DeleteCompany((new Company())))->deleteOne($id);
            return redirect()->route('company')
                ->with('message', "Company with id '$id' was successfully deleted");
        }catch(\Exception $e) {
            return redirect()->route('company')
                ->with('message', "Company with id '$id' was not deleted");
        }
    }
}
