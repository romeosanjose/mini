<?php

namespace App\Http\Controllers;

use App\Domain\Company\Company;
use App\Domain\Company\UseCase\GetCompany;
use App\Domain\Employee\UseCase\DeleteEmployee;
use App\Domain\Employee\UseCase\GetEmployee;
use App\Domain\Employee\UseCase\UpdateEmployee;
use Illuminate\Http\Request;
use App\Domain\Employee\UseCase\AddEmployee;
use App\Domain\Employee\Employee;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $employees = (new GetEmployee(new Employee()))->getAll(10);
        return view('employee.index', ['employees' => $employees]);
    }

    public function create()
    {
        $companies = (new GetCompany(new Company()))->getAll();
        return view('employee.create', ['companies' => $companies]);
    }

    public function edit($id)
    {
        $companies = (new GetCompany(new Company()))->getAll();
        $employee = (new GetEmployee(new Employee()))->getOne($id);
        return view('employee.edit', ['employee' => $employee, 'companies' => $companies]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'email'
        ]);
        $employee = (new AddEmployee(new Employee()))->addEmployee($request);
        return redirect()->route('employee')->with('message', "Employee '$employee->firstname' was successfully added");
    }

    public function update($id,Request $request)
    {
        $validatedData = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'email'

        ]);
        $employee = (new UpdateEmployee(new Employee()))->updateEmployee($id, $request);
        return redirect()->route('employee')
            ->with('message', "Employee
             '$employee->firstname' was successfully updated");
    }

    public function delete($id)
    {
        try {
            (new DeleteEmployee((new Employee())))->deleteOne($id);
            return redirect()->route('employee')
                ->with('message', "Employee with id '$id' was successfully deleted");
        }catch(\Exception $e) {
            return redirect()->route('employee')
                ->with('message', "Employee with id '$id' was not deleted");
        }
    }
}
