<?php

use Illuminate\Database\Seeder;
use App\Domain\User\Service\SaltGenerator;
use App\Domain\User\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $salt = (new SaltGenerator())->generateSalt();
        $password = User::encryptPassword($salt, 'password');
        User::create([
            'name'  => 'administrator',
            'email' => 'admin@admin.com',
            'salt'  => $salt,
            'password' => $password,
        ]);
    }
}
