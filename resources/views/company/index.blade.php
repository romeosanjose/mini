@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Company List')}}
                    <div class="col-3 float-right">
                        <a href="{{ URL::route('company.create') }}" class="btn btn-primary">{{__('create company')}}</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{__('Company Name')}}</th>
                                <th scope="col">{{__('Email')}}</th>
                                <th scope="col">{{__('Website')}}</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($companies)
                                @foreach($companies as $company)
                                <tr>
                                    <th scope="row">{{$company->id}}
                                        @if ($company->logo)
                                            <img src="/company/logo/{{ $company->logo }}" height="30px" width="30px" />
                                        @else
                                            <img src="/company/logo/pl.png" height="30px" width="30px" />
                                        @endif
                                    </th>
                                    <td>{{ $company->name }}</td>
                                    <td>{{ $company->email }}</td>
                                    <td>{{ $company->website }}</td>
                                    <td>
                                        <a href="{{ URL::route('company.edit', $company->id) }}" class="btn btn-primary">{{__('edit')}}</a>
                                    </td>
                                    <td>
                                        {!! Form::open(
                                           array(
                                               'route' => ['company.delete', $company->id],
                                               'class' => 'form',
                                               'novalidate' => 'novalidate',
                                               'files' => true)) !!}
                                        @csrf
                                        <button type="submit" value="{{__('Delete')}}" class="btn btn-primary">delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $companies->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
