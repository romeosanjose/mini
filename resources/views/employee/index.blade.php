@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Employee List')}}
                    <div class="col-3 float-right">
                        <a href="{{ URL::route('employee.create') }}" class="btn btn-primary">{{__('create employee')}}</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{__('Employee Name')}}</th>
                                <th scope="col">{{__('Email')}}</th>
                                <th scope="col">{{__('Phone')}}</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($employees)
                                @foreach($employees as $employee)
                                <tr>
                                    <th scope="row">{{$employee->id}}
                                    </th>
                                    <td>{{ $employee->lastname .','. $employee->firstname }} </td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->phone }}</td>
                                    <td>
                                        <a href="{{ URL::route('employee.edit', $employee->id) }}" class="btn btn-primary">{{__('edit')}}</a>
                                    </td>
                                    <td>
                                        {!! Form::open(
                                           array(
                                               'route' => ['employee.delete', $employee->id],
                                               'class' => 'form',
                                               'novalidate' => 'novalidate',
                                               'files' => true)) !!}
                                        @csrf
                                        <button type="submit" value="{{__('Delete')}}" class="btn btn-primary">delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $employees->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
