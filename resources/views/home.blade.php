@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Welcome!
                        <div class="col-3 float-right">
                            <a href="{{ URL::route('company') }}" class="btn btn-primary">{{__('Go to Company')}}</a>
                        </div>
                        <div class="col-3 float-right">
                            <a href="{{ URL::route('employee') }}" class="btn btn-primary">{{__('Go to Employee')}}</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
