<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('company')->group(function(){
    Route::get('/', 'CompanyController@index')->name('company');
    Route::get('/create', 'CompanyController@create')->name('company.create');
    Route::get('/edit/{id}', 'CompanyController@edit')->name('company.edit');
    Route::post('/update/{id}', 'CompanyController@update')->name('company.update');
    Route::post('/delete/{id}', 'CompanyController@delete')->name('company.delete');
    Route::post('/', 'CompanyController@store')->name('company.store');
});

Route::prefix('employee')->group(function(){
    Route::get('/', 'EmployeeController@index')->name('employee');
    Route::get('/create', 'EmployeeController@create')->name('employee.create');
    Route::get('/edit/{id}', 'EmployeeController@edit')->name('employee.edit');
    Route::post('/update/{id}', 'EmployeeController@update')->name('employee.update');
    Route::post('/delete/{id}', 'EmployeeController@delete')->name('employee.delete');
    Route::post('/', 'EmployeeController@store')->name('employee.store');
});


Route::get('/company/logo/{filename}', function ($filename)
{
    return Image::make(storage_path() . '/app/public/images/company/logo/' . $filename)->response();
});
