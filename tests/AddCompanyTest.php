<?php

use Tests\TestCase;

class AddCompanyTest extends TestCase
{
    /**
     * @dataProvider dataForCompanyCreate
     *
     */
    public function testCreateExistingUserEmail()
    {
        $mockFile = $this->getMockBuilder(Symfony\Component\HttpFoundation\File\UploadedFile::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockFile->method("getClientOriginalExtension")
            ->willReturn('.jpg');
        $mockFile->method("move")
            ->willReturn([]);

        $requestMock = $this->getMockBuilder(Illuminate\Http\Request::class)
        ->disableOriginalConstructor()
        ->getMock();
        $requestMock->name = "company1";
        $requestMock->email = "comp@mail.com";
        $requestMock->website = "test";
        $requestMock->logo = "sample.jpeg";
        $requestMock->method("file")
            ->willReturn([$mockFile]);


        $mockCompany = $this->getMockBuilder(\App\Domain\Company\Employee::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockCompany->method("save")
            ->willReturn([]);
        $companyUseCase = new \App\Domain\Company\UseCase\AddCompany($mockCompany);



        $result = $companyUseCase->AddCompany($requestMock);

        $this->assertTrue($result != null);
    }

    public function dataForCompanyCreate()
    {
        return [
            [
                [
                    'password' => '@password',
                    'email'    => 'batman@hero.com',
                    'website'   => 'samplewebsite'
                ]
            ]
        ];
    }


}
